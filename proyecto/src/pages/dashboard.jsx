import React, { useState, useEffect } from 'react';
import { ProductService } from '../pages/service/ProductService';
import { Button } from 'primereact/button';
import { DataView, DataViewLayoutOptions } from 'primereact/dataview';
import { Carousel } from 'primereact/carousel';
import { Tag } from 'primereact/tag';
import { Chip } from 'primereact/chip';
import { Dialog } from 'primereact/dialog';
import { FileUpload } from 'primereact/fileupload';
import { InputNumber } from 'primereact/inputnumber';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { RadioButton } from 'primereact/radiobutton';
import { Rating } from 'primereact/rating';
import { Toast } from 'primereact/toast';
import { SplitButton } from 'primereact/splitbutton';
import Cookies from 'js-cookie';
export default function PaginationDemo() {
    const [products, setProducts] = useState([]);
    const [layout, setLayout] = useState('grid');
    const [searchText, setSearchText] = useState('');
    const [productDialog, setProductDialog] = useState(false);
    const [loading, setLoading] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const responsiveOptions = [
        {
            breakpoint: '1199px',
            numVisible: 1,
            numScroll: 1
        },
        {
            breakpoint: '991px',
            numVisible: 2,
            numScroll: 1
        },
        {
            breakpoint: '767px',
            numVisible: 1,
            numScroll: 1
        }
    ];

    fetch('/api/apis/productos')
        .then(response => response.json())
        .then(data => {
            setProducts(data);
            setLoading(false);
        })
        .catch(error => {
            console.error('Error:', error);
            setLoading(false);
        });


    const getSeverity = (product) => {
        switch (product.StockStatus) {
            case 'En Stock':
                return 'success';
            case 'Stock Bajo':
                return 'warning';
            case 'Cero Stock':
                return 'danger';
            case 'Stock Medio':
                return 'warning';
            default:
                return null;
        }
    };
    const productTemplate = (product) => {
        return (
            <div className="border-1 surface-border border-round m-2 text-center py-5 px-3">
                <div className="mb-3">
                    <img src={`https://primefaces.org/cdn/primereact/images/product/${product.foto}`} alt={product.nombre} className="w-6 shadow-2" />
                </div>
                <div>
                    <h4 className="mb-1">{product.nombre}</h4>
                    <h6 className="mt-0 mb-3">${product.precio}</h6>
                    <Tag severity={getSeverity(product)}></Tag>
                    <div className="mt-5 flex flex-wrap gap-2 justify-content-center">
                        <Button icon="pi pi-search" className="p-button p-button-rounded" />
                        <Button icon="pi pi-star-fill" className="p-button-success p-button-rounded" />
                    </div>
                </div>
            </div>
        );
    };
    const listItem = (product) => {
        return (
            <div className="col-12">
                <div className="flex flex-column xl:flex-row xl:align-items-start p-4 gap-4">
                    <img className="w-9 sm:w-16rem xl:w-10rem shadow-2 block xl:block mx-auto border-round" src={`https://primefaces.org/cdn/primereact/images/product/${product.image}`} alt={product.name} />
                    <div className="flex flex-column sm:flex-row justify-content-between align-items-center xl:align-items-start flex-1 gap-4">
                        <div className="flex flex-column align-items-center sm:align-items-start gap-3">
                            <div className="text-2xl font-bold text-900">{product.nombre}</div>
                            <Rating value={product.rating} readOnly cancel={false}></Rating>
                            <div className="flex align-items-center gap-3">
                                <span className="flex align-items-center gap-2">
                                    <i className="pi pi-tag"></i>
                                    <span className="font-semibold">{product.tipo}</span>
                                </span>
                                <Tag value={product.StockStatus} severity={getSeverity(product)}></Tag>
                            </div>
                        </div>
                        <div className="flex sm:flex-column align-items-center sm:align-items-end gap-3 sm:gap-2">
                            <span className="text-2xl font-semibold">${product.precio}</span>
                            <Button icon="pi pi-search" className="p-button p-button-rounded" />
                            <Button icon="pi pi-shopping-cart" className="p-button-rounded" disabled={product.inventoryStatus === 'OUTOFSTOCK'}></Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
    const gridItem = (product) => {
        const handleRef = async () => {
            const referencia = product.referencia
            const noDocumento = Cookies.get('No_documento');
            const response = await fetch('/api/apis/productos', {
    
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ referencia, noDocumento })
            });
    
            if (response.ok) {
                const user = await response.json();
            } else {
                alert('Inicio de sesión fallido');
            }
        };
        return (
            <div className="col-12 sm:col-6 lg:col-12 xl:col-4 p-2">
                <div className="p-4 border-1 surface-border surface-card border-round">
                    <div className="flex flex-wrap align-items-center justify-content-between gap-2">
                        <div className="flex align-items-center gap-2">
                            <i className="pi pi-tag"></i>
                            <span className="font-semibold">{product.tipo}</span>
                        </div>
                        <Tag value={product.StockStatus} severity={getSeverity(product)}></Tag>
                    </div>
                    <div className="flex flex-column align-items-center gap-3 py-5">
                        <img className="w-9 shadow-2 border-round" src={`https://primefaces.org/cdn/primereact/images/product/${product.foto}`} alt={product.nombre} />
                        <div className="text-2xl font-bold">{product.nombre}</div>
                        <Rating value={product.rating} readOnly cancel={false}></Rating>
                    </div>
                    <div className="flex align-items-center justify-content-between">
                        <span className="text-2xl font-semibold">${product.precio}</span>
                        <Button icon="pi pi-search" className="p-button p-button-rounded" />
                        <Button icon="pi pi-shopping-cart"  className="p-button-rounded" disabled={product.StockStatus === 'Cero Stock'}></Button>
                    </div>
                </div>
            </div>
        );
    };

    const itemTemplate = (product, layout) => {
        if (!product) {
            return;
        }

        if (layout === 'list') return listItem(product);
        else if (layout === 'grid') return gridItem(product);
    };

    const header = () => {
        return (
            <div className='row'>
                <div className='col-12 d-flex justify-content-between'>
                    <span className="p-input-icon-left">
                        <i className="pi pi-search" />
                        <input
                            type="text"
                            value={searchText}
                            onChange={(e) => setSearchText(e.target.value)}
                            placeholder="Buscar producto"
                            className="p-inputtext"
                        />
                    </span>

                    <DataViewLayoutOptions layout={layout} onChange={(e) => setLayout(e.value)} />
                </div>
            </div>
        );
    };

    const filteredProducts = products.filter((product) =>
        product.nombre.toLowerCase().includes(searchText.toLowerCase())
    );
    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
      };
      const saveProduct = () => {
        setSubmitted(true);
      };
    const productDialogFooter = (
        <>
          <Button label="Cancel" icon="pi pi-times" text onClick={hideDialog} />
          <Button label="Save" icon="pi pi-check" text onClick={saveProduct} />
        </>
      );

    return (
        <div>
            <div className="card  p-4 gap-4" style={{ marginBottom: '20px' }}>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <Chip label="Los Muebles mas vendidos" icon="pi pi-caret-down" pt={{
                        root: { style: { background: 'linear-gradient(to right, #253738, #60cbd1)', borderRadius: '24px' } },
                        label: { className: 'text-white' }
                    }} />
                </div>
                <Carousel value={products} numVisible={3} numScroll={3} responsiveOptions={responsiveOptions} itemTemplate={productTemplate} />
            </div>
            <div className="card">
                {loading && <div className="text-center">Cargando...</div>}
                <DataView value={filteredProducts} itemTemplate={itemTemplate} layout={layout} header={header()} paginator rows={9} />
            </div>
            <Dialog visible={productDialog} style={{ width: '500px' }} header="Editar Perfil" modal className="p-fluid" footer={productDialogFooter} onHide={hideDialog}>
                {/*{product.image && <img src={`/demo/images/product/${product.image}`} alt={product.image} width="150" className="mt-0 mx-auto mb-5 block shadow-2" />}*/}
                <div className="field">
                    <label htmlFor="name" className="font-bold block mb-2">Tipo de Documento</label>
                    <InputText id="name" required autoFocus />
                </div>
                <div className="field">
                    <label htmlFor="description" className="font-bold block mb-2">Número de documento </label>
                    <InputText id="description" required />
                </div>
                <div className="field">
                    <label htmlFor="description" className="font-bold block mb-2">Nombre completo del cliente</label>
                    <InputText id="description" required />
                </div>
                <div className="field">
                    <label htmlFor="description" className="font-bold block mb-2">Teléfono de residencia</label>
                    <InputNumber id="description" required useGrouping={false} />
                </div>
                <div className="flex-auto">
                    <label htmlFor="withoutgrouping" className="font-bold block mb-2">Teléfono celular</label>
                    <InputNumber inputId="withoutgrouping" useGrouping={false} />
                </div>
                <div className="field">
                    <label htmlFor="description" className="font-bold block mb-2">Dirección</label>
                    <InputTextarea id="description" required rows={1} cols={20} />
                </div>
                <div className="flex-auto">
                    <label htmlFor="withoutgrouping" className="font-bold block mb-2">Ciudad de residencia</label>
                    <InputNumber inputId="withoutgrouping" useGrouping={false} />
                </div>
                <div className="flex-auto">
                    <label htmlFor="withoutgrouping" className="font-bold block mb-2">Departamento</label>
                    <InputNumber inputId="withoutgrouping" useGrouping={false} />
                </div>
                <div className="flex-auto">
                    <label htmlFor="withoutgrouping" className="font-bold block mb-2">País</label>
                    <InputNumber inputId="withoutgrouping" useGrouping={false} />
                </div>
                <div className="flex-auto">
                    <label htmlFor="withoutgrouping" className="font-bold block mb-2">Profesión</label>
                    <InputNumber inputId="withoutgrouping" useGrouping={false} />
                </div>
                <div className="flex-auto">
                    <label htmlFor="withoutgrouping" className="font-bold block mb-2">Email</label>
                    <InputNumber inputId="withoutgrouping" useGrouping={false} />
                </div>
            </Dialog>
        </div>

    );
}